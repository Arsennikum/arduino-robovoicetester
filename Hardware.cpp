#include "Hardware.h"

// ������������� ���������� ����
bool Hardware::ledOn = false;

bool Hardware::isBtn0Pressed()
{
	return digitalRead(btnConnectPin);
}

bool Hardware::isBtn1Pressed()
{
	return digitalRead(btnOpenClosePin);
}

void Hardware::setIndicatorsHigh()
{
	digitalWrite(ledPin, HIGH);
	ledOn = true;
}

void Hardware::setIndicatorsLow()
{
	digitalWrite(ledPin, LOW);
	ledOn = false;
}

void Hardware::initPins()
{
	pinMode(btnConnectPin, INPUT);
	pinMode(btnOpenClosePin, INPUT);
	pinMode(ledPin, OUTPUT);
}

void Hardware::blinksLed(int count)
{
	if (ledOn) {
		digitalWrite(ledPin, LOW);
		delay(500);
	}
	for (int i = 0; i < count; i++)
	{
		digitalWrite(ledPin, HIGH);
		delay(300);
		digitalWrite(ledPin, LOW);
		delay(300);
	}
	digitalWrite(ledPin, ledOn);

}

void Hardware::changeIndicatorsState()
{
	if (ledOn) {
		setIndicatorsLow();
	}
	else {
		setIndicatorsHigh();
	}
}



Hardware::Hardware()
{
}

Hardware::~Hardware()
{
}