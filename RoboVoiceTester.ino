#include "Message.cpp"
#include "Hardware.h"

enum msgInCode { INDICATORS_OFF = 0, INDICATORS_ON, CHANGE_INDICATORS, BLINK_LED};
enum msgErrorCode { UNKNOWN_MSG_CODE };

void setup()
{
	Serial.begin(9600);
	Hardware::initPins();
	Message::sendInfo("Start work");

}

void loop()
{
	if (Hardware::isBtn0Pressed()) {
		while (Hardware::isBtn0Pressed()) { }
		Message::sendInfo("Btn0");
		delay(5);
	}
	if (Hardware::isBtn1Pressed()) {
		while (Hardware::isBtn1Pressed()) {}
		Message::sendInfo("Btn1");
		delay(5);
	}
	BluetoothController();
}

void BluetoothController() {
	if (Serial.available() == 0) {
		return; // �����, ���� ��� ������� ������
	}

	String readedString = Serial.readString();

	msgInCode MsgCode = (msgInCode)(readedString[0] - '0');

	switch (MsgCode)
	{
	case INDICATORS_ON: {
		Hardware::setIndicatorsHigh();
		Message::sendResultOk(MsgCode);
		break;
	}
	case INDICATORS_OFF: {
		Hardware::setIndicatorsLow();
		Message::sendResultOk(MsgCode);
		break;
	}
	case CHANGE_INDICATORS: {
		Hardware::changeIndicatorsState();
		Message::sendResultOk(MsgCode);
		break;
	}
	case BLINK_LED: {
		int blinksCount;
		blinksCount = readedString.substring(1, readedString.length()).toInt(); // if convert error return 0
		Hardware::blinksLed(blinksCount);
		Message::sendResultOk(MsgCode);
		break;
	}
	default: {
		Message::sendResultFail(MsgCode, msgErrorCode::UNKNOWN_MSG_CODE);
		break;
	}
	}

}