// 
// 
// 
#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class Message
{
public:
	static enum msgOutCode { OK = 0, FAIL, INFO, NEW_USER_KEYS, NEW_KEY } MsgOutCode;

	static void sendNewKey(String key) {
		SendProtocolMessage(NEW_KEY, key);
	}
	
	static void sendInfo(String msg) {
		SendProtocolMessage(INFO, msg);
	}
	
	static void sendKeysNewUser(int id, String key) {
		String idStr = String(id);
		if (idStr.length() < 2) {
			idStr = "0" + idStr;
		}
		String msg = idStr + key;
		SendProtocolMessage(NEW_USER_KEYS, msg);
	}
	
	static void sendResultOk(int operationCode) {
		SendProtocolMessage(OK, (String)operationCode);
	}
	
	static void sendResultFail(int operationCode) {
		SendProtocolMessage(FAIL, (String)operationCode);
	}

	static void sendResultFail(int operationCode, int errorCode) {
		SendProtocolMessage(FAIL, (String)operationCode + (String)errorCode);
	}

private:
	Message() {}
	~Message() {}
	static void SendProtocolMessage(msgOutCode code, String data)
	{
		String msg = (int)code + data;
		Serial.println(msg);
	};
};