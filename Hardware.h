// Hardware.h

#ifndef _HARDWARE_h
#define _HARDWARE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


#endif

class Hardware
{
public:
	static bool isBtn0Pressed();
	static bool isBtn1Pressed();
	static void setIndicatorsHigh();
	static void setIndicatorsLow();
	static void initPins();
	static void blinksLed(int count);
	static void changeIndicatorsState();

private:
	static const int btnOpenClosePin = 2;
	static const int btnConnectPin = 3;
	static const int ledPin = 7;
	static bool ledOn;
	Hardware();
	~Hardware();
};